import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Login from "./components/Login";
import SignUp from "./components/Register";
import Home from "./components/Home";
import Admin from './components/Admin';
import EditProduct from './components/EditProduct';
function App() {
  return (
    <Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/home"}>Trang chủ</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-in"}>Đăng nhập</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-up"}>Đăng ký</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="outer">
      <div className="inner">
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/home' component={Home} />
           
            <Route path="/sign-in" component={Login} />
               
            <Route path="/sign-up" component={SignUp} />
            <Route path="/admin" component={Admin} />
            <Route path="/edit-product" component={EditProduct} />
          </Switch>
          </div>
      </div>
    </div></Router>
  );
}

export default App;
