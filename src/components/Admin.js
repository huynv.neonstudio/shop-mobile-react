import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import api from "../api";

const Admin = (props) => {
    console.log(props)
    const history = useHistory()
    const [listTool, setlistTool] = useState([]);
    const [listProduct, setlistProduct] = useState([]);
    const getAllProduct =async () => {
        const result = await api.getListProduct({})
        
        console.log(result)
        setlistProduct(result.data)
    }
    const gotoUpdateTool = (productId)=>{
        history.push({
            pathname: '/edit-product',
            state: {  // location state
                productId: productId, 
              },
        })
    }
    useEffect(() => {
        getAllProduct()
    }, [])
    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="main-box clearfix">
                            <div className="table-responsive">
                                <table className="table user-list">
                                    <thead>
                                        <tr>
                                            <th><span>STT</span></th>
                                            <th><span>Image</span></th>
                                            <th className="text-center"><span>Name</span></th>
                                            <th><span>Description</span></th>
                                            {/* <th><span>Create date</span></th> */}
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {listProduct&&listProduct.map((e, index) =>
                                            <tr key={index}>
                                                <td>
                                                    {index + 1}
                                                </td>
                                                <td>
                                                    <img src={e.image} alt="" />
                                                    <a href="#" className="user-link">{e.name}</a>
                                                    <span className="user-subhead">{e.title}</span>
                                                </td>
                                                <td>
                                                    {e.name}
                                                </td>
                                                <td className="text-center">
                                                {e.descriptions}
                                                </td>
                                                {/* <td>
                                                    {(new Date(e.createdAt)).toLocaleDateString('en-GB')}
                                                </td> */}
                                                <td style={{ width: '20%' }}>
                                                    <button onClick={()=>gotoUpdateTool(e._id)} className="btn btn-warning">
                                                      Edit
                                                    </button>
                                                   
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Admin;