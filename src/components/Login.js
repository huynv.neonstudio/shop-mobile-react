import React from 'react';
import api from './../api/index';
import { useHistory } from "react-router-dom";
const { useEffect, useState } = React;
const Login = (props) => {
   const history = useHistory()
   const [username,setusername] = useState("")
   const [password,setpassword] = useState("")
   const [loading,setloading] = useState(false)
   const login = async () =>{
    setloading(true)
    const result = await api.login({
        username : username,
        password : password
    })
    setloading(false)
    if(result.isSuccess)
    {
        localStorage.setItem("user_auth",JSON.stringify(result.data))
        history.push('/home')
    }
   }
    return (
    <form>
        <h3>Đăng nhập</h3>
        <div className="form-group">
            <label>Email</label>
            <input  onChange={(e)=>setusername(e.target.value)} type="email" className="form-control" placeholder="Nhập email" />
        </div>

        <div className="form-group">
            <label>Password</label>
            <input  onChange={(e)=>setpassword(e.target.value)} type="password" className="form-control" placeholder="Nhập mật khẩu" />
        </div>

        <div className="form-group">
            <div className="custom-control custom-checkbox">
                <input  type="checkbox" className="custom-control-input" id="customCheck1" />
                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
            </div>
        </div>

        <button type="button" onClick={login} className="btn btn-dark btn-lg btn-block">Đăng nhập 
            {loading&&<span style={{marginLeft : 10}} className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
        </button>
        <p className="forgot-password text-right">
            Forgot <a href="#">password?</a>
        </p>
    </form>)
}
export default Login