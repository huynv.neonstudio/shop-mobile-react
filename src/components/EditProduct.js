import { useEffect, useState } from "react";

import { useLocation } from 'react-router-dom';
import api from "../api";
const EditProduct = (props) => {
    const location = useLocation();
    console.log(location.state)
    const [productInfo, setproductInfo] = useState({});
    const nameChange = (data) => {
        setproductInfo({ ...productInfo, name: data })
    }
    const imageChange = (data) => {
        setproductInfo({ ...productInfo, image: data })
    }
    const DesChange = (data) => {
        setproductInfo({ ...productInfo, descriptions: data })
    }
    const getProductInfo = async ()=>{
        const productResult = await api.getInfoProduct({
            _id : location.state.productId
        })
        console.log(productResult)
        if(productResult.isSuccess)
        {
            setproductInfo(productResult.data)
        }
    }
    const updateProductInfo = async () =>{
        console.log("9403289434")
            const resultUpdateProduct = await api.updateProduct(productInfo)
            console.log(resultUpdateProduct)
            if(resultUpdateProduct.isSuccess)
            {
                alert("Success")
            }else{
                alert(resultUpdateProduct.message)
            }
    }
    useEffect(()=>{
        getProductInfo()
    },[])
    return (
        <>
            <div className="container" style={{ marginTop: 20 }}>
                <div className="row">
                    <div className="form-group col-md-6" style={{ marginTop: 20 }}>
                        <label>Tên </label>
                        <input value={productInfo.name} onChange={(data) => nameChange(data.target.value)} className="form-control" type="text" name="name" />
                    </div>
                    <div className="form-group col-md-6" style={{ marginTop: 20 }}>
                        <label>Link Ảnh </label>
                        <input value={productInfo.image} onChange={(data) => imageChange(data.target.value)} className="form-control" type="text" name="name" />
                    </div>
                    <div className="form-group col-md-12" style={{ marginTop: 20 }}>
                        <label>Mô tả </label>
                        <input value={productInfo.descriptions} onChange={(data) => DesChange(data.target.value)} className="form-control" type="text" name="name" />
                    </div>
                    <div className="form-group col-md-2" style={{ marginTop: 20 }}>
                    <button onClick={updateProductInfo}  className="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default EditProduct