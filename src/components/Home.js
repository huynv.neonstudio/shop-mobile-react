import { useEffect, useState } from "react"
import api from "../api"

const Home = (props) =>{
    const [userInfo,setuserInfo] = useState({})

    const getUserInfo = async () =>{
        const resultGetUserInfo = await api.getUserInfo({})
        console.log(resultGetUserInfo)
        setuserInfo(resultGetUserInfo.data)
    }
    useEffect(()=>{
        getUserInfo()
    },[])
    return(
        <div>Tôi là : <b>{userInfo?userInfo.username:''}</b></div>
        )
}
export default Home