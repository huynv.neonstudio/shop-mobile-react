const Register = (props) => {
    return <form>
        <h3>Register</h3>

        <div className="form-group">
            <label>Họ và tên</label>
            <input type="text" className="form-control" placeholder="Họ và tên" />
        </div>

        <div className="form-group">
            <label>Tên đăng nhập</label>
            <input type="text" className="form-control" placeholder="Nhập username" />
        </div>

        <div className="form-group">
            <label>Email</label>
            <input type="email" className="form-control" placeholder="Nhập email" />
        </div>

        <div className="form-group">
            <label>Mật khẩu</label>
            <input type="password" className="form-control" placeholder="Nhập password" />
        </div>

        <button type="submit" className="btn btn-dark btn-lg btn-block">Đăng ký</button>
        <p className="forgot-password text-right">
            Already registered <a href="#">log in?</a>
        </p>
    </form>
}
export default Register