import * as apiLinks from '../constants/apiLinks'
import request from './makeRequest'
const api ={
    getUserInfo         :      (data => request.post(apiLinks.API_USER_GET_INFO,data) ),
    login               :      (data => request.post(apiLinks.API_USER_LOGIN,data) ),
    getListProduct         :      (data => request.post(apiLinks.API_PRODUCT_LIST,data) ),
    getInfoProduct         :      (data => request.post(apiLinks.API_PRODUCT_VIEW,data) ),
    updateProduct         :      (data => request.post(apiLinks.API_PRODUCT_EDIT,data) ),
    
    }  
export default api
